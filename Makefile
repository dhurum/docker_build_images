GCOVR_VERSION=4.1

all: basecpp sjparser-build

gcovr-${GCOVR_VERSION}.tar.gz:
	wget https://github.com/gcovr/gcovr/archive/${GCOVR_VERSION}.tar.gz
	mv ${GCOVR_VERSION}.tar.gz gcovr-${GCOVR_VERSION}.tar.gz

basecpp: gcovr-${GCOVR_VERSION}.tar.gz
	docker build \
		-t dhurum/basecpp:latest \
		-f base/Dockerfile \
		.

sjparser-build:
	docker build \
		-t dhurum/sjparser-build:latest \
		--build-arg BASE_REGISTRY=dhurum \
		--build-arg BASE_TAG=latest \
		-f sjparser/Dockerfile \
		.
